/**
 * A class for mocking tests of the ShipperService
 * 
 * Note the extra comments here are for training purposes ONLY
 * 
 * In production these comments would not be included.
 */
package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import com.example.demo.entities.Shipper;
import com.example.demo.repository.ShipperRepository;

@ActiveProfiles("h2")
@SpringBootTest
public class ShipperServiceTest {

	// Get a "real" shipperService from the spring container
	@Autowired
	ShipperService shipperService;

	// Get a "mock" or "fake" shipper repository
	// from the spring container
	@MockBean
	ShipperRepository shipperRepository;
	
	@Test
	public void testGetShipper() {
		// create a shipper record for testing
		int testId = 5;
		String testName = "testShipper";
		Shipper testShipper = new Shipper();
		testShipper.setId(testId);
		testShipper.setName(testName);
		
		// tell the mock object what to do when
		// its getShipperById method is called
		when(shipperRepository.getShipperById(testShipper.getId()))
		.thenReturn(testShipper);
		
		// call the getShipper method on the service, this will call
		// the getShipperById method on the the mock repository
		// the mock repository returns the testShipper
		// the service should return the same testShipper here
		// we verify this happens, so we know the service is behaving as expected
		assertThat(shipperService.getShipper(testId)).isEqualTo(testShipper);
	}
}
